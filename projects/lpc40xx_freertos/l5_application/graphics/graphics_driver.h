#pragma once
#include "game_timer.h"
#include <stdint.h>

void init_graphics(void);

void display_score(uint32_t score);

void move_player_left(void);

void move_player_right(void);

void player_chop_left(void);

void player_chop_right(void);

void player_collision_with_tree(int player_position);

void display_game_start(void);

void display_start_screen(void);

void display_game_over(void);

void clear_branch(int row, int col);

void display_branch(int row, int col);

void display_timer_start(void);

void clear_timer(void);

void update_timer_task(void);
