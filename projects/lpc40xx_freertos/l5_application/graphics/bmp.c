#include "bmp.h"
#include "ff.h"
#include "led_display_driver.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

static unsigned int bytes_read;
static void read_file_header(BMPFile_t *file);
static void read_image_header(BMPFile_t *file);
static FATFS FatFs;

FRESULT open_bmp(BMPFile_t *file) {
  f_mount(&FatFs, "", 0); // subsequent calls re-initialize the filesystem
  FRESULT ret = f_open(&(file->file_handle), file->filename, FA_READ);
  return ret;
}

void read_bmp(BMPFile_t *file) {
  // check if file has been opened before
  if (file->file_handle.flag == 0) {
    open_bmp(file);
  }
  read_file_header(file);

  read_image_header(file);

  const int img_header_byte_padding = 2;
  const int bytes_to_read = file->image_header.image_size_bytes + img_header_byte_padding;
  // size BMPFile_t buffer after reading image header
  file->bmp_buffer = (uint8_t *)calloc((bytes_to_read), sizeof(uint8_t));

  // read image array
  for (int i = 0; i < bytes_to_read; i++) {
    f_read(&(file->file_handle), &file->bmp_buffer[i], sizeof(uint8_t), &bytes_read);
  }
  close_bmp(file);
}

void close_bmp(BMPFile_t *file) { f_close(&(file->file_handle)); }

void print_bmp_header(BMPFile_t *file) {
  printf("header: %c %c\n", (file->file_header.header_field & 0xFF), (file->file_header.header_field >> 8));
  printf("file size: %ld\n", file->file_header.file_size);
  printf("bmp start offset: %ld bytes\n", file->file_header.bitmap_starting_offset);
}

void print_bmp_image_header(BMPFile_t *file) {
  printf("DIB size: %ld\n", file->image_header.size);
  printf("image width: %ld\n", file->image_header.width);
  printf("image height: %ld\n", file->image_header.height);
  printf("number of color planes: %d\n", file->image_header.num_color_planes);
  printf("bits per pixel: %d\n", file->image_header.bits_per_pixel);
  printf("compression type: %8lX\n", file->image_header.compression);
  printf("image size: %ld bytes\n", file->image_header.image_size_bytes);
  printf("x ppm: %ld\n", file->image_header.x_ppm);
  printf("y ppm: %ld\n", file->image_header.y_ppm);
  printf("num of colors: %ld\n", file->image_header.num_colors);
  printf("important colors: %ld\n", file->image_header.important_colors);
}

void print_bmp_buffer(BMPFile_t *file) {
  for (int i = 0; i < file->image_header.image_size_bytes; i++) {
    printf("Byte %d: %d\n", i, file->bmp_buffer[i]);
  }
}

void display_64x64_bmp_to_array(BMPFile_t *file) {
  int buffer_index = 2; // important! need to start @ 2 b/c of byte padding
  for (int row = 63; row >= 0; row--) {
    for (int col = 63; col >= 0; col--) {
      color_e bmp_pixel = 0;
      (file->bmp_buffer[buffer_index++]) ? bmp_pixel |= BLUE : bmp_pixel;
      (file->bmp_buffer[buffer_index++]) ? bmp_pixel |= GREEN : bmp_pixel;
      (file->bmp_buffer[buffer_index++]) ? bmp_pixel |= RED : bmp_pixel;
      // printf("row: %d\tcol: %d\tpixel: %d\n", row, col, bmp_pixel);
      draw_pixel(row, col, bmp_pixel);
    }
  }
}

void display_bmp_to_array(int row, int col, BMPFile_t *file) {
  int buffer_index = 2; // important! need to start @ 2 b/c of byte padding from header
  const int end_row = row;
  const int end_col = 63 - (col + (file->image_header.width - 1));
  const int start_row = row + (file->image_header.height - 1);
  const int start_col = 63 - col;

  // calc if there are padding bytes, bmp store image rows in multiple of 4 bytes
  int padding_bytes = 0;
  int padding_required = (file->image_header.width * 3) % 4;
  (padding_required) ? padding_bytes = 4 - padding_required : padding_bytes;

  for (int led_row = start_row; led_row >= end_row; led_row--) {
    for (int led_col = start_col; led_col >= end_col; led_col--) {
      color_e bmp_pixel = 0;
      (file->bmp_buffer[buffer_index++]) ? bmp_pixel |= BLUE : bmp_pixel;
      (file->bmp_buffer[buffer_index++]) ? bmp_pixel |= GREEN : bmp_pixel;
      (file->bmp_buffer[buffer_index++]) ? bmp_pixel |= RED : bmp_pixel;
      // printf("row: %d\tcol: %d\n", led_row, led_col);
      draw_pixel(led_row, led_col, bmp_pixel);
    }
    buffer_index += padding_bytes;
  }
}

void display_bmp_score_to_array(int row, int col, BMPFile_t *file) {
  int buffer_index = 2; // important! need to start @ 2 b/c of byte padding from header
  const int end_row = row;
  const int end_col = 63 - (col + (file->image_header.width - 1));
  const int start_row = row + (file->image_header.height - 1);
  const int start_col = 63 - col;

  // calc if there are padding bytes, bmp store image rows in multiple of 4 bytes
  int padding_bytes = 0;
  int padding_required = (file->image_header.width * 3) % 4;
  (padding_required) ? padding_bytes = 4 - padding_required : padding_bytes;

  for (int led_row = start_row; led_row >= end_row; led_row--) {
    for (int led_col = start_col; led_col >= end_col; led_col--) {
      color_e bmp_pixel = 0;
      (file->bmp_buffer[buffer_index++]) ? bmp_pixel |= BLUE : bmp_pixel;
      (file->bmp_buffer[buffer_index++]) ? bmp_pixel |= GREEN : bmp_pixel;
      (file->bmp_buffer[buffer_index++]) ? bmp_pixel |= BLUE : bmp_pixel;
      // printf("row: %d\tcol: %d\n", led_row, led_col);
      draw_pixel(led_row, led_col, bmp_pixel);
    }
    buffer_index += padding_bytes;
  }
}

// static helper functions---------------------------------------------------------------------------------

void read_file_header(BMPFile_t *file) {
  int ret = FR_OK;
  (ret == FR_OK) ? f_read(&(file->file_handle), &file->file_header.header_field, sizeof(file->file_header.header_field),
                          &bytes_read)
                 : ret;
  (ret == FR_OK)
      ? f_read(&(file->file_handle), &file->file_header.file_size, sizeof(file->file_header.file_size), &bytes_read)
      : ret;
  (ret == FR_OK)
      ? f_read(&(file->file_handle), &file->file_header.reserved, sizeof(file->file_header.reserved), &bytes_read)
      : ret;
  (ret == FR_OK)
      ? f_read(&(file->file_handle), &file->file_header.reserved2, sizeof(file->file_header.reserved2), &bytes_read)
      : ret;
  (ret == FR_OK) ? f_read(&(file->file_handle), &file->file_header.bitmap_starting_offset,
                          sizeof(file->file_header.bitmap_starting_offset), &bytes_read)
                 : ret;
}

void read_image_header(BMPFile_t *file) {
  int ret = FR_OK;
  (ret == FR_OK) ? f_read(&(file->file_handle), &file->image_header.size, sizeof(file->image_header.size), &bytes_read)
                 : ret;
  (ret == FR_OK)
      ? f_read(&(file->file_handle), &file->image_header.width, sizeof(file->image_header.width), &bytes_read)
      : ret;
  (ret == FR_OK)
      ? f_read(&(file->file_handle), &file->image_header.height, sizeof(file->image_header.height), &bytes_read)
      : ret;
  (ret == FR_OK) ? f_read(&(file->file_handle), &file->image_header.num_color_planes,
                          sizeof(file->image_header.num_color_planes), &bytes_read)
                 : ret;
  (ret == FR_OK) ? f_read(&(file->file_handle), &file->image_header.bits_per_pixel,
                          sizeof(file->image_header.bits_per_pixel), &bytes_read)
                 : ret;
  (ret == FR_OK) ? f_read(&(file->file_handle), &file->image_header.compression, sizeof(file->image_header.compression),
                          &bytes_read)
                 : ret;
  (ret == FR_OK) ? f_read(&(file->file_handle), &file->image_header.image_size_bytes,
                          sizeof(file->image_header.image_size_bytes), &bytes_read)
                 : ret;
  (ret == FR_OK)
      ? f_read(&(file->file_handle), &file->image_header.x_ppm, sizeof(file->image_header.x_ppm), &bytes_read)
      : ret;
  (ret == FR_OK)
      ? f_read(&(file->file_handle), &file->image_header.y_ppm, sizeof(file->image_header.y_ppm), &bytes_read)
      : ret;
  (ret == FR_OK)
      ? f_read(&(file->file_handle), &file->image_header.num_colors, sizeof(file->image_header.num_colors), &bytes_read)
      : ret;
  (ret == FR_OK) ? f_read(&(file->file_handle), &file->image_header.num_color_planes,
                          sizeof(file->image_header.num_color_planes), &bytes_read)
                 : ret;
}