#pragma once

#include "ff.h"
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// 14 bytes
typedef struct BmpFileHeader_s {
  uint16_t header_field;           // type "B" "M" for bmp
  uint32_t file_size;              // file size in bytes
  uint16_t reserved;               // not used
  uint16_t reserved2;              // not used
  uint32_t bitmap_starting_offset; // offset to image data in bytes from start of file

} BmpFileHeader_t;

typedef struct BmpImageHeader_s {
  uint32_t size;             // Device Independent Bitmap (DIB) header size in bytes
  int32_t width;             // image width
  int32_t height;            // image height
  uint16_t num_color_planes; // number of color planes
  uint16_t bits_per_pixel;   // bits per pixel
  uint32_t compression;      // compression type
  uint32_t image_size_bytes; // image size in bytes
  int32_t x_ppm;             // pixels per meter
  int32_t y_ppm;             // pixels per meter
  uint32_t num_colors;       // number of colors
  uint32_t important_colors; // important colors
} BmpImageHeader_t;

typedef struct BMPFile_s {
  char filename[30];
  FIL file_handle;
  BmpFileHeader_t file_header;
  BmpImageHeader_t image_header;
  uint8_t *bmp_buffer;
} BMPFile_t;

// bmp pixel array of image stored bottom up
// starting bottom left corner of image, left to right, bottom to top
FRESULT open_bmp(BMPFile_t *file);

void read_bmp(BMPFile_t *file);

void close_bmp(BMPFile_t *file);

void print_bmp_header(BMPFile_t *file);

void print_bmp_image_header(BMPFile_t *file);

void print_bmp_buffer(BMPFile_t *file);

void display_64x64_bmp_to_array(BMPFile_t *file);

void display_bmp_to_array(int row, int col, BMPFile_t *file);

void display_bmp_score_to_array(int row, int col, BMPFile_t *file);
