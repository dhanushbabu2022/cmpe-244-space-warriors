#include "graphics_driver.h"
#include "bmp.h"
#include "delay.h"
#include "led_display_driver.h"
#include <stdio.h>
#include <string.h>

static BMPFile_t integer_bmp[] = {{"0.bmp", 0, 0, 0, NULL}, {"1.bmp", 0, 0, 0, NULL}, {"2.bmp", 0, 0, 0, NULL},
                                  {"3.bmp", 0, 0, 0, NULL}, {"4.bmp", 0, 0, 0, NULL}, {"5.bmp", 0, 0, 0, NULL},
                                  {"6.bmp", 0, 0, 0, NULL}, {"7.bmp", 0, 0, 0, NULL}, {"8.bmp", 0, 0, 0, NULL},
                                  {"9.bmp", 0, 0, 0, NULL}};

static BMPFile_t player_left = {"char_stand_left.bmp", 0, 0, 0, NULL};
static BMPFile_t chop_left = {"char_chop_left.bmp", 0, 0, 0, NULL};
static BMPFile_t player_right = {"char_stand_right.bmp", 0, 0, 0, NULL};
static BMPFile_t chop_right = {"char_chop_right.bmp", 0, 0, 0, NULL};
static BMPFile_t start_screen = {"start_screen.bmp", 0, 0, 0, NULL};
static BMPFile_t game_start = {"game_start.bmp", 0, 0, 0, NULL};
static BMPFile_t game_over = {"game_over.bmp", 0, 0, 0, NULL};

// function prototypes
static void display_score_hundred(int hundreds);
static void display_score_tens(int tens);
static void display_score_ones(int ones);
static void init_player_left_graphics(void);
static void init_player_right_graphics(void);
static void init_start_screen_graphics(void);
static void init_game_start_graphics(void);
static void init_game_over_graphics(void);
static void init_score_graphics(void);
static void blink_timer(int sec_left, int blink_duration_ms);
static void draw_timer_left(int sec_left);

void init_graphics(void) {
  init_player_left_graphics();
  init_player_right_graphics();
  init_start_screen_graphics();
  init_game_start_graphics();
  init_game_over_graphics();
  init_score_graphics();
}

void init_player_left_graphics(void) {
  read_bmp(&player_left);
  read_bmp(&chop_left);
}

void init_player_right_graphics(void) {
  read_bmp(&player_right);
  read_bmp(&chop_right);
}

void init_start_screen_graphics(void) { read_bmp(&start_screen); }

void init_game_start_graphics(void) { read_bmp(&game_start); }

void init_game_over_graphics(void) { read_bmp(&game_over); }

void init_score_graphics(void) {
  for (int i = 0; i < 10; i++) {
    read_bmp(&integer_bmp[i]);
  }
}

void display_score(uint32_t score) {
  // ensure input doesn't exceed 4 decimal places
  while (score > 999) {
    score /= 10;
  }

  // hundred
  score %= 1000;
  int hundreds = score / 100;
  display_score_hundred(hundreds);

  //   // tens
  score %= 100;
  int tens = score / 10;
  display_score_tens(tens);

  //   // ones
  score %= 10;
  int ones = score / 1;
  display_score_ones(ones);
}

void display_start_screen(void) { display_64x64_bmp_to_array(&start_screen); }

void display_game_start(void) { display_64x64_bmp_to_array(&game_start); }

void display_game_over(void) { display_64x64_bmp_to_array(&game_over); }

void move_player_left(void) {
  const int row = 48, col = 16;
  display_bmp_to_array(row, col, &player_left);
}

void move_player_right(void) {
  const int row = 48, col = 39;
  display_bmp_to_array(row, col, &player_right);
}

void player_chop_left(void) {
  const int row = 48, col = 16;
  display_bmp_to_array(row, col, &chop_left);
}

void player_chop_right(void) {
  const int row = 48, col = 39;
  display_bmp_to_array(row, col, &chop_right);
}

void player_collision_with_tree(int player_position) {
  // player flashes on/off before switching to game over screen
  if (player_position == 0) {
    move_player_left();
    delay__ms(70);
    clear_display_section(48, 16, 9, 10);
    move_player_left();
    delay__ms(70);
    clear_display_section(48, 16, 9, 10);
    move_player_left();
    delay__ms(70);
    clear_display_section(48, 16, 9, 10);
  } else {
    move_player_right();
    delay__ms(70);
    clear_display_section(48, 39, 9, 10);
    move_player_right();
    delay__ms(70);
    clear_display_section(48, 39, 9, 10);
    move_player_right();
    delay__ms(70);
    clear_display_section(48, 39, 9, 10);
  }
}

// We can fit 5 branches on each side of the display
// Each branch 4 pixels high, 20 pixels wide, 5 pixels spacing between each row of branch
//  branch 0 is the lowest branch closest to the player

// Right Side----------------------
// branch R4 -- row 8, col 39
// branch R3 -- row 17, col 39
// branch R2 -- row 26, col 39
// branch R1 -- row 35, col 39
// branch R0 -- row 44, col 39

// Left Side----------------------
// branch L4 -- row 8, col 5
// branch L3 -- row 17, col 5
// branch L2 -- row 26, col 5
// branch L1 -- row 35, col 5
// branch L0 -- row 44, col 5

void clear_branch(int row, int col) { clear_display_section(row, col, 19, 3); }

void display_branch(int row, int col) {
  for (int led_row = row; led_row < row + 3; led_row++) {
    for (int led_col = col; led_col < col + 19; led_col++) {
      draw_pixel(led_row, led_col, RED);
    }
  }
}

// 2 x 20 pixel timer
void display_timer_start(void) {
  const int start_row = 60;
  const int end_row = 61;
  const int start_col = 22;
  const int end_col = 41;
  for (int row = start_row; row < end_row; row++) {
    for (int col = start_col; col < end_col; col++) {
      draw_pixel(row, col, GREEN);
    }
  }
}

void update_timer_task(void) {
  while (1) {
    int timer = get_time();
    int time_left = timer / 1000;
    switch (time_left) {
    case 0:
      clear_timer();
      break;
    case 1:
      blink_timer(time_left, 80);
      break;
    case 2:
      blink_timer(time_left, 150);
      break;
    case 3:
      blink_timer(time_left, 250);
      break;
    case 4:
    case 5:
    case 6:
    case 7:
    case 8:
    case 9:
      clear_timer();
      draw_timer_left(time_left);
      break;
    }
    vTaskDelay(100);
  }
}

// static helper functions ---------------------------------------------------------------------------------
void display_score_hundred(int hundreds) {
  const int row = 44, col = 27;
  display_bmp_score_to_array(row, col, &integer_bmp[hundreds]);
}

void display_score_tens(int tens) {
  const int row = 44, col = 31;
  display_bmp_score_to_array(row, col, &integer_bmp[tens]);
}

void display_score_ones(int ones) {
  const int row = 44, col = 35;
  display_bmp_score_to_array(row, col, &integer_bmp[ones]);
}

static void blink_timer(int sec_left, int blink_duration_ms) {
  clear_timer();
  delay__ms(blink_duration_ms);
  draw_timer_left(sec_left);
}

void clear_timer(void) {
  const int start_row = 60;
  const int end_row = 61;
  const int start_col = 22;
  const int end_col = 41;
  for (int row = start_row; row < end_row; row++) {
    for (int col = start_col; col < end_col; col++) {
      draw_pixel(row, col, BLACK);
    }
  }
}

static void draw_timer_left(int sec_left) {
  const int start_row = 60;
  const int end_row = 61;
  const int start_col = 22;
  // clears a 2x2 pixel area from left to right as timer runs down
  const int end_col = 41 - ((10 - sec_left) * 2);
  ;
  for (int row = start_row; row < end_row; row++) {
    for (int col = start_col; col < end_col; col++) {
      draw_pixel(row, col, GREEN);
    }
  }
}
