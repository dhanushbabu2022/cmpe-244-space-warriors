#include "game_timer.h"
#include "FreeRTOS.h"
#include "semphr.h"
#include "task.h"
#include <stdint.h>
#include <stdio.h>

// If player does not press any input, the game will be over in 10 seconds
static const int timer_start = 10000; // 10,000 milliseconds
static volatile int timer = timer_start;

// average reaction time is ~273 milliseconds
static const uint32_t timer_plus = 250;
static const uint32_t timer_minus = 100;

void reset_timer(void) { timer = timer_start; }

void start_timer(void *p) {
  while (1) {
    vTaskDelay(100);
    if (timer > 0) {
      timer -= timer_minus;
      printf("timer: %d\n", timer);
    } else {
      timer = 0;
    }
  }
}

void extend_time(void) {
  if ((timer > 0) && (timer < timer_start)) {
    timer += timer_plus;
  }
}

bool check_if_time_out(void) {
  bool time_out = true;
  if (timer > 0) {
    time_out = false;
  }
  return time_out;
}

int get_time(void) { return timer; }