#pragma once
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>

void reset_timer(void);

void start_timer(void *p);

void extend_time(void);

bool check_if_time_out(void);

int get_time(void);
