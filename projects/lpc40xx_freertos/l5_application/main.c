#include <stdio.h>

#include "FreeRTOS.h"
#include "task.h"

#include "board_io.h"
#include "common_macros.h"
#include "lpc40xx.h"
#include "periodic_scheduler.h"
#include "semphr.h"
#include "sj2_cli.h"

#include "bmp.h"
#include "delay.h"
#include "game_logic.h"
#include "game_timer.h"
#include "graphics_driver.h"
#include "led_display_driver.h"
#include "mp3_dec.h"

TaskHandle_t bg_music;
TaskHandle_t game_over_music;
TaskHandle_t timer_task_handle;
TaskHandle_t timer_display_handle;
TaskHandle_t game_score_handle;

static SemaphoreHandle_t start_button_signal;
static SemaphoreHandle_t continue_game;

static game_state_e state = START_SCREEN;

static void led_display_test(void);
static void move_and_chop_left(void);
static void move_and_chop_right(void);
static void game_logic__init(void);
static void play_mp3_music_bg(void);
static void play_mp3_music_hit(void);
static void play_mp3_music_game_over(void);

static void display_test__task(void *p);
static void left_button__task(void *p);
static void right_button__task(void *p);
static void start_button__task(void *p);
static void game_state_machine__task(void *p);

void score_task(void *p);

int main(void) {
  continue_game = xSemaphoreCreateBinary();
  start_button_signal = xSemaphoreCreateBinary();

  game_buttons__init();
  led_display_init();
  led_display_test();
  mp3_decoder__init();
  mp3_decoder__volume_set_level(25);
  game_logic__init();

  vTaskStartScheduler();
  return 0;
}

void led_display_test(void) { xTaskCreate(display_test__task, "display", 4096, NULL, PRIORITY_MEDIUM, NULL); }

void display_test__task(void *p) {
  while (1) {
    update_display();
    vTaskDelay(1);
  }
}

void game_logic__init(void) {
  xTaskCreate(game_state_machine__task, "Game State Machine", 4096 / sizeof(void *), NULL, PRIORITY_MEDIUM, NULL);
  xTaskCreate(left_button__task, "Left button", 1024 / sizeof(void *), NULL, PRIORITY_LOW, NULL);
  xTaskCreate(right_button__task, "Right button", 1024 / sizeof(void *), NULL, PRIORITY_LOW, NULL);
  xTaskCreate(start_button__task, "Start button", 1024 / sizeof(void *), NULL, PRIORITY_LOW, NULL);
  xTaskCreate(start_timer, "Game Timer", 1024 / sizeof(void *), NULL, PRIORITY_LOW, &timer_task_handle);
  vTaskSuspend(timer_task_handle);
  xTaskCreate(update_timer_task, "timer display", 1024 / sizeof(void *), NULL, PRIORITY_LOW, &timer_display_handle);
  vTaskSuspend(timer_display_handle);
  xTaskCreate(score_task, "Score", 2048 / sizeof(void *), NULL, PRIORITY_LOW, &game_score_handle);
  vTaskSuspend(game_score_handle);
}

void game_state_machine__task(void *p) {
  init_graphics();
  xSemaphoreGive(start_button_signal);
  while (1) {
    switch (state) {
    case START_SCREEN: {
      display_start_screen();
      play_mp3_music_bg();
      state = IDLE;
      break;
    }

    case GAME_START: {
      xSemaphoreGive(continue_game);
      vTaskResume(timer_task_handle);
      vTaskResume(timer_display_handle);
      vTaskResume(game_score_handle);
      reset_timer();
      reset__game_score();
      initialize_branch_queue();
      clear_all_branches();
      display_game_start();
      display_timer_start();
      update_branch_display();
      move_player_right();
      state = IDLE;
      break;
    }

    case GAME_OVER: {
      vTaskSuspend(timer_task_handle);
      vTaskSuspend(timer_display_handle);
      play_mp3_music_game_over();
      clear_timer();
      display_game_over();
      xSemaphoreGive(start_button_signal); // lets start button restart game again
      xSemaphoreTake(continue_game, 0);    // prevent left and right buttons from working in game over state
      state = PLAY_AGAIN_IDLE;
      break;
    }

    case CHOP_LEFT: {
      play_mp3_music_hit();
      move_and_chop_left();
      clear_all_branches();
      update_branch_display();
      update_score();
      extend_time();
      state = CHECK_STATE;
      break;
    }

    case CHOP_RIGHT: {
      play_mp3_music_hit();
      move_and_chop_right();
      clear_all_branches();
      update_branch_display();
      update_score();
      extend_time();
      state = CHECK_STATE;
      break;
    }

    case CHECK_STATE: {

      if (check_collision_branch() == get__player_position()) {
        state = GAME_OVER;
      } else {
        state = IDLE;
      }
      break;
    }

    case IDLE: {
      vTaskDelay(100);
      if (check_if_time_out()) {
        state = GAME_OVER;
      }
      break;
    }

    case PLAY_AGAIN_IDLE: {
      vTaskDelay(100);
      break;
    }
    }
  }
}

void start_button__task(void *p) {
  while (1) {
    if (check_button_status(get__button_start())) {
      if (xSemaphoreTake(start_button_signal, 0)) {
        state = GAME_START;
      }
    }
    vTaskDelay(100);
  }
}

void left_button__task(void *p) {
  while (1) {
    if (check_button_status(get__button_left())) {
      if (xSemaphoreTake(continue_game, 0)) {
        state = CHOP_LEFT;
      }
    }
    vTaskDelay(100);
  }
}

void right_button__task(void *p) {
  while (1) {
    if (check_button_status(get__button_right())) {
      if (xSemaphoreTake(continue_game, 0)) {
        state = CHOP_RIGHT;
      }
    }
    vTaskDelay(100);
  }
}

void move_and_chop_left(void) {
  set__player_position(0);
  insert_to_queue();
  chop_left__graphics();
  xSemaphoreGive(continue_game);
}

void move_and_chop_right(void) {
  set__player_position(1);
  insert_to_queue();
  chop_right__graphics();
  xSemaphoreGive(continue_game);
}

void score_task(void *p) {
  while (1) {
    display_score(get__game_score());
    vTaskDelay(250);
  }
}

void play_mp3_music_bg(void) { mp3_decoder__play_song_at_index(02); }

void play_mp3_music_game_over(void) { mp3_decoder__play_song_at_index(01); }

void play_mp3_music_hit(void) { mp3_decoder__play_song_at_index(03); }
