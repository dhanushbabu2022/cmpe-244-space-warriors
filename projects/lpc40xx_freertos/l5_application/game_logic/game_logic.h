#pragma once

#include <stdbool.h>
#include <stdio.h>

#include "delay.h"
#include "gpio.h"
#include "graphics_driver.h"
#include "led_display_driver.h"

#define MAX_TREE_SIZE (6)

typedef enum {
  START_SCREEN, // 0
  GAME_START,   // 1
  GAME_OVER,    // 2
  CHOP_LEFT,    // 3
  CHOP_RIGHT,   // 4
  CHECK_STATE,  // 5
  IDLE,         // 6
  PLAY_AGAIN_IDLE
} game_state_e;

uint8_t get__player_position(void);
void set__player_position(uint8_t position_value);
uint32_t get__game_score(void);
void reset__game_score(void);
uint32_t get__time_count(void);

gpio_s get__button_left(void);
gpio_s get__button_right(void);
gpio_s get__button_start(void);

bool check_button_status(gpio_s button);
void game_buttons__init(void);
void insert_to_queue(void);
void initialize_branch_queue(void);

/*
* Right Side----------------------
// branch R4 -- row 8, col 39
// branch R3 -- row 17, col 39
// branch R2 -- row 26, col 39
// branch R1 -- row 35, col 39
// branch R0 -- row 44, col 39

* Left Side----------------------
// branch L4 -- row 8, col 5
// branch L3 -- row 17, col 5
// branch L2 -- row 26, col 5
// branch L1 -- row 35, col 5
// branch L0 -- row 44, col 5
*/
void update_branch_display(void);
void clear_all_branches(void);
void update_score(void);
uint8_t check_collision_branch(void);
void chop_left__graphics(void);
void chop_right__graphics(void);
