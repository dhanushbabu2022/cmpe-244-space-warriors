#include "game_logic.h"
#include <stdlib.h>
#include <time.h>

static volatile int queue_array[MAX_TREE_SIZE] = {-1, -1, -1, -1, -1, -1};
static const int branch_row[MAX_TREE_SIZE - 1] = {8, 17, 26, 35, 44};

/*PLAYER POSITION
 * 0 ==> LEFT
 * 1 ==> RIGHT
 */
static volatile uint8_t player_position = 0;
static volatile uint32_t game_score = 0;
static volatile uint32_t time_count = 100;

static const gpio_s button_left = {.port_number = GPIO__PORT_2, .pin_number = 5};
static const gpio_s button_right = {.port_number = GPIO__PORT_2, .pin_number = 7};
static const gpio_s start_button = {.port_number = GPIO__PORT_2, .pin_number = 2};

bool check_button_status(gpio_s button) { return (gpio__get(button)); }

void game_buttons__init(void) {
  gpio__construct_as_input(button_left.port_number, button_left.pin_number);
  gpio__enable_pull_down_resistors(button_left);
  gpio__construct_as_input(button_right.port_number, button_right.pin_number);
  gpio__enable_pull_down_resistors(button_right);
  gpio__construct_as_input(start_button.port_number, start_button.pin_number);
  gpio__enable_pull_down_resistors(start_button);
}

uint8_t get__player_position(void) { return player_position; }
void set__player_position(uint8_t position_value) { player_position = position_value; }
uint32_t get__game_score(void) { return game_score; }
void reset__game_score(void) { game_score = 0; }
uint32_t get__time_count(void) { return time_count; }

gpio_s get__button_left(void) { return button_left; }
gpio_s get__button_right(void) { return button_right; }
gpio_s get__button_start(void) { return start_button; }

void insert_to_queue(void) {
  queue_array[5] = queue_array[4];
  queue_array[4] = queue_array[3];
  queue_array[3] = queue_array[2];
  queue_array[2] = queue_array[1];
  queue_array[1] = queue_array[0];
  queue_array[0] = (rand() & 1);
}

void initialize_branch_queue(void) {
  for (int i = 0; i < MAX_TREE_SIZE - 1; i++) {
    insert_to_queue();
  }
  queue_array[5] = -1;
}

void update_branch_display(void) {
  for (int i = 0; i < 5; ++i) {
    if (queue_array[i]) {
      display_branch(branch_row[i], 6);
    } else {
      display_branch(branch_row[i], 39);
    }
  }
}

void clear_all_branches(void) {
  for (int i = 0; i < 5; ++i) {
    clear_branch(branch_row[i], 39);
    clear_branch(branch_row[i], 6);
  }
}

void update_score(void) { game_score++; }

uint8_t check_collision_branch(void) { return queue_array[MAX_TREE_SIZE - 1]; }

void chop_left__graphics(void) {
  clear_display_section(48, 39, 9, 10);
  move_player_left();
  delay__ms(50);
  clear_display_section(48, 16, 9, 10);
  player_chop_left();
  delay__ms(50);
  clear_display_section(48, 16, 9, 10);
  move_player_left();
}

void chop_right__graphics(void) {
  clear_display_section(48, 16, 9, 10);
  move_player_right();
  delay__ms(50);
  clear_display_section(48, 39, 9, 10);
  player_chop_right();
  delay__ms(50);
  clear_display_section(48, 39, 9, 10);
  move_player_right();
}