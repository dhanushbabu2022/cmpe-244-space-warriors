#if (0)
#include "FreeRTOS.h"
#include "board_io.h"
#include "delay.h"
#include "gpio.h"
#include "mp3_dec.h"
#include "queue.h"
#include "semphr.h"
#include "task.h"
#include "uart.h"
#include <stdbool.h>
#include <stdio.h>
#include <string.h>

void play_mp3_music_bg_task(void *p);
void play_mp3_music_game_over_task(void *q);
void play_mp3_music_hit_task(void *r);

int main(void) {

  mp3_decoder__init();
  xTaskCreate(play_mp3_music_bg_task, "play_music", (2 * 1024) / sizeof(void *), NULL, PRIORITY_MEDIUM, NULL);
  xTaskCreate(play_mp3_music_game_over_task, "play_music", (2 * 1024) / sizeof(void *), NULL, PRIORITY_HIGH, NULL);
  xTaskCreate(play_mp3_music_hit_task, "play_music", (2 * 1024) / sizeof(void *), NULL, PRIORITY_LOW, NULL);

  puts("Starting RTOS");
  vTaskStartScheduler();
  return 0;
}

void play_mp3_music_bg_task(void *p) {

  mp3_decoder__volume_set_level(22);
  mp3_decoder__play_song_at_index(02);
  // puts(" No switch press ");
  // vTaskDelay(30000000);
}

void play_mp3_music_game_over_task(void *q) {

  mp3_decoder__volume_set_level(25);
  mp3_decoder__play_song_at_index(01);
}

void play_mp3_music_hit_task(void *r) {

  mp3_decoder__volume_set_level(25);
  mp3_decoder__play_song_at_index(03);
}

#endif