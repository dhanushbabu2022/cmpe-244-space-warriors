#pragma once
#include "gpio.h"

typedef enum { BLACK = 0, BLUE = 1, GREEN = 2, CYAN = 3, RED = 4, PURPLE = 5, YELLOW = 6, WHITE = 7 } color_e;

void led_display_init(void);

void draw_pixel(int row, int col, color_e color);

void clear_pixel(int row, int col);

void update_display(void);

void clear_display(void);

void clear_display_row(int row);

void clear_display_col(int col);

void clear_display_section(int row, int col, int width, int height);

// updating display
// 1. read updated values from mem to pixel buffer
//      * read memory
//      * draw_pixel() for new values
// 2. call display_pixels to show new image

// TODO -- API's usable by graphics driver
// graphics driver is abstraction on top of display driver
// * generates character pixel values and indexes (row,column)
// * generate game background pixel values and indexes
// * updates character / environment graphics