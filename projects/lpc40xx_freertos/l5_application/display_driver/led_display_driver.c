#include "led_display_driver.h"
#include "FreeRTOS.h"
#include "delay.h"
#include "gpio.h"
#include "semphr.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef union display_interface_u {
  struct display_interface_s {
    gpio_s clock;
    gpio_s latch;
    gpio_s output;
    gpio_s R1; // controls rows 0 - 31
    gpio_s G1;
    gpio_s B1;
    gpio_s R2; // controls rows 32 - 63
    gpio_s G2;
    gpio_s B2;
    gpio_s A;
    gpio_s B;
    gpio_s C;
    gpio_s D;
    gpio_s E;
  } gpio_pins;
  gpio_s display_interface[14];
} display_interface_t;

// constants
static const int num_gpio_pins = 14;
static const int num_rows = 32;
static const int pixels_per_row = 64;
static const int num_cols = pixels_per_row;
static const int row_min = 0;
static const int row_max = 64;
static const int col_min = row_min;
static const int col_max = row_max;

static SemaphoreHandle_t buffer_muxtex; // buffer mutex
static display_interface_t led_matrix;

// 32x64 display buffer, each 8-bit pixel values holds
// row i and row i+32 pixel RGB values
// 8 bit pixel data
// 7  6  5  4  | 3  2  1  0
// 0  R2 G2 B2 | 0  R1 G2 B1
static volatile uint8_t pixel_buffer[64][64];

// static function prototypes
static void display_gpio_construct_as_IO(void);
static void display_gpio_set_as_output(void);
static void reset_all_gpio_to_low(void);

static void output_disable(void);
static void output_enable(void);
static void latch_enable(void);
static void latch_disable(void);
static void clock_data_in(void);
static void select_row(int row);

static void clear_row_selection(void);

static bool row_in_bounds(int row);
static bool col_in_bounds(int col);

void led_display_init(void) {
  //   buffer_muxtex = xSemaphoreCreateMutex();

  // Set the used gpio as IO first, in case their functions were changed by any startup code
  display_gpio_construct_as_IO();
  display_gpio_set_as_output();

  // Set everything to known state OFF
  reset_all_gpio_to_low();
}

// 8 bit pixel data
// 7  6  5  4  | 3  2  1  0
// 0  R2 G2 B2 | 0  R1 G2 B1
// takes row  [0,63]
// takes col  [0,63]
void draw_pixel(int row, int col, color_e color) {
  // clear pixel previous value  before writing new one
  clear_pixel(row, col);

  if (!(row_in_bounds(row)) || !(col_in_bounds(col))) {
    return;
  }
  pixel_buffer[row][col] = color;
}

// takes row  [0,63]
// takes col  [0,63]
void clear_pixel(int row, int col) {
  uint8_t clear_pixel = 0x0F;
  if (!(row_in_bounds(row)) || !(col_in_bounds(col))) {
    return;
  }
  pixel_buffer[row][col] = 0;
}

/* DISPLAY INDEXING
 * (0x0) Top left
 * Columns 0 - - - - >63
 * Rows _________________
 *   0  | | | | | | | | |
 *   |  -----------------
 *   |  | | | | | | | | |
 *   V  -----------------
 *   63                   (63,63) Bottom right
 */
void update_display(void) {

  for (int row = 0; row < num_rows; row++) {
    latch_disable();
    output_disable();
    select_row(row);

    // Shifting in MSB first
    for (int col = num_cols - 1; col >= 0; col--) {
      (pixel_buffer[row][col] & 0x1) ? gpio__set(led_matrix.gpio_pins.B1) : gpio__reset(led_matrix.gpio_pins.B1);
      (pixel_buffer[row][col] & 0x2) ? gpio__set(led_matrix.gpio_pins.G1) : gpio__reset(led_matrix.gpio_pins.G1);
      (pixel_buffer[row][col] & 0x4) ? gpio__set(led_matrix.gpio_pins.R1) : gpio__reset(led_matrix.gpio_pins.R1);

      (pixel_buffer[row + 32][col] & 0x1) ? gpio__set(led_matrix.gpio_pins.B2) : gpio__reset(led_matrix.gpio_pins.B2);
      (pixel_buffer[row + 32][col] & 0x2) ? gpio__set(led_matrix.gpio_pins.G2) : gpio__reset(led_matrix.gpio_pins.G2);
      (pixel_buffer[row + 32][col] & 0x4) ? gpio__set(led_matrix.gpio_pins.R2) : gpio__reset(led_matrix.gpio_pins.R2);

      clock_data_in();
    }
    latch_enable();  // latch new data to output reg
    output_enable(); // output and light LED's
    delay__us(50);   // increase delay to make screen brighter
  }
  output_disable(); // helps fix rows 31, 63 from appearing brighter than the other lines
}

void clear_display(void) { memset(pixel_buffer, 0, sizeof(pixel_buffer)); }

// takes row argument [0,63]
void clear_display_row(int row) {
  if (!row_in_bounds(row)) {
    return;
  }
  for (int col = 0; col < num_cols; col++) {
    clear_pixel(row, col);
  }
}

// takes col argument [0,63]
void clear_display_col(int col) {
  if (!col_in_bounds(col)) {
    return;
  }
  for (int row = 0; row < num_rows; row++) {
    pixel_buffer[row][col] = BLACK; // clears all 8 bits
  }
}

void clear_display_section(int row, int col, int width, int height) {
  const int end_row = row + height;
  const int end_col = col + width;
  if (!(row_in_bounds(row)) || !(row_in_bounds(end_row)) || !(col_in_bounds(col)) || !(col_in_bounds(end_row))) {
    return;
  }
  for (int led_row = row; led_row < end_row; led_row++) {
    for (int led_col = col; led_col < end_col; led_col++) {
      pixel_buffer[led_row][led_col] = BLACK;
    }
  }
}

// static functions
void output_disable(void) { gpio__set(led_matrix.gpio_pins.output); }

void output_enable(void) { gpio__reset(led_matrix.gpio_pins.output); }

void latch_enable(void) { gpio__set(led_matrix.gpio_pins.latch); }

void latch_disable(void) { gpio__reset(led_matrix.gpio_pins.latch); }

void clock_data_in(void) {
  gpio__set(led_matrix.gpio_pins.clock);
  gpio__reset(led_matrix.gpio_pins.clock);
}

void select_row(int row) {
  (row & 0x01) ? gpio__set(led_matrix.gpio_pins.A) : gpio__reset(led_matrix.gpio_pins.A);
  (row & 0x02) ? gpio__set(led_matrix.gpio_pins.B) : gpio__reset(led_matrix.gpio_pins.B);
  (row & 0x04) ? gpio__set(led_matrix.gpio_pins.C) : gpio__reset(led_matrix.gpio_pins.C);
  (row & 0x08) ? gpio__set(led_matrix.gpio_pins.D) : gpio__reset(led_matrix.gpio_pins.D);
  (row & 0x10) ? gpio__set(led_matrix.gpio_pins.E) : gpio__reset(led_matrix.gpio_pins.E);
}

void clear_row_selection(void) {
  gpio__reset(led_matrix.gpio_pins.A);
  gpio__reset(led_matrix.gpio_pins.B);
  gpio__reset(led_matrix.gpio_pins.C);
  gpio__reset(led_matrix.gpio_pins.D);
  gpio__reset(led_matrix.gpio_pins.E);
}

bool row_in_bounds(int row) { return ((row >= row_min) && (row < row_max)); }

bool col_in_bounds(int col) { return ((col >= col_min) && (col < col_max)); }

void display_gpio_construct_as_IO(void) {
  // control pins
  // led_matrix.gpio_pins.clock = gpio__construct_with_function(GPIO__PORT_0, 15, GPIO__FUNCITON_0_IO_PIN);
  // led_matrix.gpio_pins.latch = gpio__construct_with_function(GPIO__PORT_0, 22, GPIO__FUNCITON_0_IO_PIN);
  // led_matrix.gpio_pins.output = gpio__construct_with_function(GPIO__PORT_0, 18, GPIO__FUNCITON_0_IO_PIN);

  // // colors
  // led_matrix.gpio_pins.R1 = gpio__construct_with_function(GPIO__PORT_0, 8, GPIO__FUNCITON_0_IO_PIN);
  // led_matrix.gpio_pins.G1 = gpio__construct_with_function(GPIO__PORT_0, 26, GPIO__FUNCITON_0_IO_PIN);
  // led_matrix.gpio_pins.B1 = gpio__construct_with_function(GPIO__PORT_1, 31, GPIO__FUNCITON_0_IO_PIN);

  // led_matrix.gpio_pins.R2 = gpio__construct_with_function(GPIO__PORT_0, 9, GPIO__FUNCITON_0_IO_PIN);
  // led_matrix.gpio_pins.G2 = gpio__construct_with_function(GPIO__PORT_0, 25, GPIO__FUNCITON_0_IO_PIN);
  // led_matrix.gpio_pins.B2 = gpio__construct_with_function(GPIO__PORT_1, 30, GPIO__FUNCITON_0_IO_PIN);

  // // mux pins
  // led_matrix.gpio_pins.A = gpio__construct_with_function(GPIO__PORT_2, 0, GPIO__FUNCITON_0_IO_PIN);
  // led_matrix.gpio_pins.B = gpio__construct_with_function(GPIO__PORT_2, 2, GPIO__FUNCITON_0_IO_PIN);
  // led_matrix.gpio_pins.C = gpio__construct_with_function(GPIO__PORT_2, 5, GPIO__FUNCITON_0_IO_PIN);
  // led_matrix.gpio_pins.D = gpio__construct_with_function(GPIO__PORT_2, 7, GPIO__FUNCITON_0_IO_PIN);
  // led_matrix.gpio_pins.E = gpio__construct_with_function(GPIO__PORT_2, 9, GPIO__FUNCITON_0_IO_PIN);

  // control pins
  led_matrix.gpio_pins.clock = gpio__construct_with_function(GPIO__PORT_2, 1, GPIO__FUNCITON_0_IO_PIN);
  led_matrix.gpio_pins.latch = gpio__construct_with_function(GPIO__PORT_2, 0, GPIO__FUNCITON_0_IO_PIN);
  led_matrix.gpio_pins.output = gpio__construct_with_function(GPIO__PORT_2, 4, GPIO__FUNCITON_0_IO_PIN);
  // colors
  led_matrix.gpio_pins.R1 = gpio__construct_with_function(GPIO__PORT_0, 7, GPIO__FUNCITON_0_IO_PIN);
  led_matrix.gpio_pins.G1 = gpio__construct_with_function(GPIO__PORT_0, 6, GPIO__FUNCITON_0_IO_PIN);
  led_matrix.gpio_pins.B1 = gpio__construct_with_function(GPIO__PORT_0, 9, GPIO__FUNCITON_0_IO_PIN);
  led_matrix.gpio_pins.R2 = gpio__construct_with_function(GPIO__PORT_0, 25, GPIO__FUNCITON_0_IO_PIN);
  led_matrix.gpio_pins.G2 = gpio__construct_with_function(GPIO__PORT_0, 26, GPIO__FUNCITON_0_IO_PIN);
  led_matrix.gpio_pins.B2 = gpio__construct_with_function(GPIO__PORT_1, 30, GPIO__FUNCITON_0_IO_PIN);
  // mux pins
  led_matrix.gpio_pins.A = gpio__construct_with_function(GPIO__PORT_1, 23, GPIO__FUNCITON_0_IO_PIN);
  led_matrix.gpio_pins.B = gpio__construct_with_function(GPIO__PORT_1, 20, GPIO__FUNCITON_0_IO_PIN);
  led_matrix.gpio_pins.C = gpio__construct_with_function(GPIO__PORT_1, 29, GPIO__FUNCITON_0_IO_PIN);
  led_matrix.gpio_pins.D = gpio__construct_with_function(GPIO__PORT_1, 28, GPIO__FUNCITON_0_IO_PIN);
  led_matrix.gpio_pins.E = gpio__construct_with_function(GPIO__PORT_1, 31, GPIO__FUNCITON_0_IO_PIN);
}

void display_gpio_set_as_output(void) {
  for (int pin = 0; pin < num_gpio_pins; pin++) {
    gpio__set_as_output(led_matrix.display_interface[pin]);
  }
}

void reset_all_gpio_to_low(void) {
  for (int pin = 0; pin < num_gpio_pins; pin++) {
    gpio__reset(led_matrix.display_interface[pin]);
  }
}